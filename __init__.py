import bl00mbox, audio

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
from st3m.ui.view import View, ViewManager

from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
from st3m.ui import colours

global APP_DIR
APP_DIR = "/flash/sys/apps/CriCri-moosetracker"
global IMAGE_FILE
IMAGE_FILE = "outdoor.png"

print(f"{APP_DIR}")


class MooseTracker(Application):
    channel: bl00mbox.Channel
    samples: list[bl00mbox.sampler] = []
    sample_names: list[str] = ["moose-low.wav", "moose-normal.wav", "moose-high.wav"]
    is_loading: bool = True

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.blm = bl00mbox.Channel("MooseTracker")
        audio.speaker_set_volume_dB(audio.speaker_get_maximum_volume_dB())

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        if self.is_loading:
            self._load_next_sample()
            return
        for idx, petal in enumerate(self.input.captouch.petals):
            if petal.whole.pressed:
                self.mooses[idx % len(self.mooses)].signals.trigger.start()

    def draw_loading(self, ctx: Context) -> None:
        ctx.rgb(*colours.BLACK)
        ctx.rectangle(-120.0, -120.0, 240.0, 240.0).fill()
        ctx.font = "Camp Font 1"
        if len(self.samples) != len(self.sample_names):
            ctx.rgb(*colours.GO_GREEN)
            ctx.text_baseline = ctx.MIDDLE
            ctx.text_align = ctx.CENTER
            ctx.move_to(0, 0)
            ctx.font_size = 20
            ctx.font = "Camp Font 1"
            ctx.text(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")
            print(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")

            progress = len(self.samples) / len(self.sample_names)

            ctx.rectangle(-80, 40, 160, 20)
            ctx.rgb(1, 1, 1)
            ctx.fill()
            ctx.rectangle(-79, 41, 158, 18)
            ctx.rgb(0, 0, 0)
            ctx.fill()

            ctx.rectangle(-79, 41, int(158 * progress), 18)
            ctx.rgb(0, 1, 0)
            ctx.fill()

    def draw(self, ctx: Context) -> None:
        if self.is_loading:
            self.draw_loading(ctx)
        else:
            ctx.image(f"{APP_DIR}/{IMAGE_FILE}", -120, -120, 240, 240)

    def _load_next_sample(self) -> None:
        loaded = len(self.samples)
        if loaded >= len(self.sample_names):
            self.is_loading = False
            return
        i = loaded
        sample = self.channel.new(bl00mbox.patches.sampler, self.sample_names[i - 1])
        sample.signals.output = self.channel.mixer
        self.samples.append(sample)


if __name__ == "__main__":
    st3m.run.run_view(MooseTracker(ApplicationContext()))
